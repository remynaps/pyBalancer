from server import dieServer
from server import yellowServer
from http.server import BaseHTTPRequestHandler, HTTPServer
import sys
from multiprocessing import Process
import http.client

if sys.argv[1:]:
    numservers = int(sys.argv[1])
else:
    numservers = 2

balancer_port = 8000
first_server = 8080
curiterator = 0


class balancer(BaseHTTPRequestHandler):
    servers = []
    cur_server = first_server

    def do_GET(self):
        global curiterator
        conn = \
            http.client.\
            HTTPConnection('127.0.0.1:'+str(self.servers[curiterator]))
        print(self.servers[curiterator])
        headers = {'Content-type': 'text/html'}

        conn.request('GET', '', '', headers)

        response = conn.getresponse()
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        # Send the html message
        self.wfile.write(response.read())

        if curiterator == len(self.servers) - 1:
            curiterator = 0
            print("reset")
        else:
            self.cur_server = self.cur_server + curiterator
            curiterator += 1
        return


def start_server(port, it):
    if it % 2 == 0:
        server = HTTPServer(('', port), yellowServer)
    else:
        server = HTTPServer(('', port), dieServer)
    print ('Started httpserver on port ' + str(port))
    server.serve_forever()

try:
    processes = []
    for x in range(0, numservers):
        p = Process(target=start_server, args=(first_server+x, x))
        p.start()
        processes.append(p)
        balancer.servers.append(first_server + x)
    load_balancer = HTTPServer(('', balancer_port), balancer)
    load_balancer.serve_forever()

except KeyboardInterrupt:
    print('^C received, shutting down the load balancer')
    for p in processes:
        p.join()
