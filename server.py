#!/usr/bin/python
from http.server import BaseHTTPRequestHandler, HTTPServer


class dieServer(BaseHTTPRequestHandler):

    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        # Send the html message
        self.wfile.write('<body bgcolor="#FF0000">\
        <h1>Hello world!</h1>\
        </body>'.encode())
        return


class yellowServer(BaseHTTPRequestHandler):

    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        # Send the html message
        self.wfile.write('<body bgcolor="#ffff00">\
        <h1>Hello world!</h1>\
        </body>'.encode())
        return
